package com.crazymakercircle.priority.threadpool;

import com.crazymakercircle.threadpool.DiscardPolicy;
import com.crazymakercircle.threadpool.RunnableWrapper;
import com.crazymakercircle.threadpool.SimpleThreadPool;
import com.crazymakercircle.util.ThreadUtil;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

// 定义一个测试用例类
public class PriorityThreadPoolTest {
    public static void main(String[] args) throws InterruptedException {
        // 创建一个优先级线程池
        PriorityThreadPoolExecutor threadPool = new PriorityThreadPoolExecutor(2,
                /*Runtime.getRuntime().availableProcessors() * 2 + 1*/ 4,
                60,
                TimeUnit.SECONDS,
                new ThreadUtil.CustomThreadFactory("PriorityThreadPool"),
                new ThreadPoolExecutor.CallerRunsPolicy());

        //提交优先级任务
        for (int i = 0; i < 10; i++) {
            threadPool.execute(new PriorityTask(new CallableWrapper(i), TaskPriority.LOW.getValue()));
        }
        for (int i = 100; i < 110; i++) {
            threadPool.execute(new PriorityTask(new CallableWrapper(i), TaskPriority.MEDIUM.getValue()));
        }
        for (int i = 1000; i < 1010; i++) {
            threadPool.execute(new PriorityTask(new CallableWrapper(i), TaskPriority.HIGH.getValue()));
        }
        // 柔和关闭线程池
        threadPool.shutdown();
        // awaitTermination + shutdown 配合使用
        // 等等  20S
        threadPool.shutdown();
        threadPool.awaitTermination(200,TimeUnit.SECONDS);
    }
}