package com.crazymakercircle.httpclient.common.codec;


import com.crazymakercircle.httpclient.common.ProtocolException;

public class EncodeException extends ProtocolException {
    public EncodeException() {
    }

    public EncodeException(String message) {
        super(message);
    }

    public EncodeException(String message, Throwable cause) {
        super(message, cause);
    }

    public EncodeException(Throwable cause) {
        super(cause);
    }
}
